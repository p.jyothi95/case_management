class Client < ApplicationRecord
  has_many :cases

  validates :name, :email, presence: true

  def self.total_tats_today_count
    all.collect(&:get_tat_today_count).sum
  end

  def get_tat_today_count
    cases.where(received_date: Date.today - tat.days).where.not(status: 'approved').count
  end

  def self.total_tats_tomorrow_count
    all.collect(&:get_tat_tomorrow_count).sum
  end

  def get_tat_tomorrow_count
    cases.where(received_date: Date.tomorrow - tat.days).where.not(status: 'approved').count
  end

  def self.total_out_of_tats_count
    all.collect(&:get_out_of_tats_count).sum
  end

  def get_out_of_tats_count
    cases.where("received_date < ?", Date.today - tat.days).where.not(status: 'approved').count
  end

  def self.total_tats_today
    all.collect(&:get_tat_today).sum
  end

  def get_tat_today
    cases.where(received_date: Date.today - tat.days).where.not(status: 'approved')
  end

  def self.total_tats_tomorrow
    all.collect(&:get_tat_tomorrow).sum
  end

  def get_tat_tomorrow
    cases.where(received_date: Date.tomorrow - tat.days).where.not(status: 'approved')
  end


  def self.total_out_of_tats
    all.collect(&:get_out_of_tats).sum
  end

  def get_out_of_tats
    cases.where("received_date < ?", Date.today - tat.days).where.not(status: 'approved')
  end
end
