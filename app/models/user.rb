class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_many :cases
  has_many :attachments
  has_many :case_investigations

  validates :name, :email, :role,  presence: true

  has_attached_file :photo,
  storage: :cloudinary,
  path: ':id/:style/:filename'
  #validates_attachment_file_name :file, :matches => [/png\Z/, /jpe?g\Z/, /gif\Z/, /pdf\Z/]
  do_not_validate_attachment_file_type :photo

  ROLES = ['admin', 'staff', 'executive']
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
