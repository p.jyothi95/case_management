class Attachment < ApplicationRecord
  belongs_to :user
  belongs_to :case
  has_attached_file :document, storage: :cloudinary, path: ':id/:style/:filename'
  has_attached_file :cropped_image, storage: :cloudinary, path: ':id/:style/:filename'

  #validates_attachment_file_name :file, :matches => [/png\Z/, /jpe?g\Z/, /gif\Z/, /pdf\Z/]
  do_not_validate_attachment_file_type :document

  def crop(x, y, w, h)
  	rand_string = SecureRandom.hex
  	file_path = "/tmp/#{rand_string}"
  	FileUtils.mkdir_p file_path
  	File.open(File.join(file_path, document_file_name), 'wb'){|f| f.puts open(document.url).read}

  	image_list = Magick::ImageList.new(open(File.join(file_path, document_file_name)))
  	cropd_image = image_list.crop(x.to_f, y.to_f, w.to_f, h.to_f)
  	cropped_image_file_name = "cropped_image-#{Time.now.to_i}.png"
  	cropd_image.write(File.join(file_path, cropped_image_file_name))

  	self.cropped_image = File.open(File.join(file_path, cropped_image_file_name))
  	self.save

  	FileUtils.rm_rf file_path
  end
end
