class Case < ApplicationRecord

  STATUSES = {'open' => 'Open', 'in_progress' => 'In Progress', 'verification_completed' => 'Verification Completed', 'approved' => 'Approved'}
  CASE_TYPES = {'address' => 'Address Verification', 'employment' => 'Employment Verification'}

  belongs_to :client
  belongs_to :user, optional: true
  has_many :attachments
  has_many :case_investigations

  validates_uniqueness_of :client_ref_no

  scope :open, -> {where(status: 'open')}

  def out_of_tat?
    return false if self.received_date.nil?

    (self.received_date+self.client.tat.days) < Date.today
  end


  def self.to_csv
    attributes = %w{client_ref_no candidate_name address}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |c|
        csv << attributes.map{ |attr| c.send(attr) }
      end
    end
  end

end 
