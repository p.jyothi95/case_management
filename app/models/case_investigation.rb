class CaseInvestigation < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :case

  BUILDING_TYPES = {'apartment' => 'Apartment', 'independent_house' => 'Independent house'}
  AREA_TYPES = {'residential' => 'Residential', 'commercial' => 'Commercial'}
  ACCOMMODATIONS = {'rented' => 'Rented', 'owned' => 'Owned', 'hostel_pg' => 'Hostel or Paying Guest'}
  ADDRESS_TYPES = {'present' => 'Present', 'permanent' => 'Permanent', 'previous' => 'Previous'}
  ADDRESS_PROOFS = {'gas_bill' => 'Gas Bill', 'ration_card' => 'Ration Card', 'voter_id' => 'Voter ID', 'water_bill' => 'Water BIll', 'Electricity_bill' => 'Electricity Bill', 'driving_license' => 'Driving License', 'passport' => 'Passport', 'bank_passbook' => 'Bank Passbook'}
  UNTRACEABLE_REASONS = {'address_incomplete' => 'Address Incomplete', 'address_untraceable' => 'Address Untraceable', 'insufficient_document' => 'Insufficient Document', 'left_job' => 'Left the job', 'no_service_area' => 'NO Service Area'}
  

  store_accessor :extras

end
