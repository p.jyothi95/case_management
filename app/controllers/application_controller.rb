class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  protect_from_forgery with: :null_session #exception

  protected
    def after_sign_in_path_for(resource)
      resource.role == 'executive' ? executive_cases_path : staff_dashboard_path
    end

end
