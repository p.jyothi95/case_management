class Staff::UsersController < ApplicationController

  def new
    @user = User.new()
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        redirect_to staff_users_path
        else
        render action: :new
      end
    end 
  end

  def index
    @users = User.all.order(created_at: :asc)
  end


  def update
    @user = User.find(params[:id])
    @user.update_attributes(user_params)
    render 'edit'
  end 


  def edit
    @user = User.find(params[:id])
  end

  private

    def user_params
      params.require(:user).permit(:name, :l_name, :dob, :id_proof, :photo,  :email, :password, :phone, :address, :role, :password_confirmation)
    end

end
