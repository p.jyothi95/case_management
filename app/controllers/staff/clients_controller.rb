class Staff::ClientsController < ApplicationController

  def new
    @client = Client.new
  end

  def create
    @client = Client.new(client_params)
    if @client.save
      redirect_to staff_clients_path
    else
      render action: :new
    end
  end

  def index
    @clients = Client.all.order(created_at: :asc)
  end

  def update
    @client = Client.find(params[:id])
    if @client.update_attributes(client_params)
        flash[:success]="Successfully updated"
        redirect_to staff_clients_path()
      else
        render 'edit'
    end
  end 

  def edit
    @client = Client.find(params[:id])
  end

  private

    def client_params
      params.require(:client).permit(:name,:email,:client_format)
    end
end
