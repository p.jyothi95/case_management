class Staff::AttachmentsController < ApplicationController
  before_action :find_case
  before_action :find_attachment

  def show
  end

  private
    def find_case
      @case = Case.find params[:case_id]
    end

    def find_attachment
      @attachment = @case.attachments.find params[:id]
    end
end
