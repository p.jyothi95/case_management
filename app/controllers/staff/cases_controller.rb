class Staff::CasesController < ApplicationController
  require 'csv'

  before_action :find_case, only: %w(show crop_attachment)

  include UploadMappings

  def index
    @users = User.where(role: 'executive')
    @clients =Client.all
    @cases = Case.where.not(status: 'approved').order(created_at: :desc)
  end

  def tats
  end

  def out_of_tats
    @users = User.where(role: 'executive')
    @cases = Client.total_out_of_tats
    render action: :index
  end

  def today_tats 
    @users = User.where(role: 'executive')
    @cases = Client.total_tats_today
    render action: :index
  end

  def tomorrow_tats
    @users = User.where(role: 'executive')
    @cases = Client.total_tats_tomorrow
    render action: :index
  end

  def upload_file
    @clients = Client.all
  end

  def upload_mapping
    @clients = Client.all
    if params[:csv].present?
      @csv = CSV.parse(params[:csv].read, headers: true)
      @client = Client.find(params[:client_id])
      fn = "upload_mapping_#{@client.client_format}_#{params[:case_type]}"
      if respond_to?(fn)
        if (@cases = send(fn))
          session[:cases] = @cases
        else
          render action: :upload_file
        end
      else
        flash[:notice] = "Selected type is not supported"
        render action: :upload_file
      end
    else
      flash[:notice] = "Please upload the file"
      render action: :upload_file
    end
  end

  def upload
    session[:cases].each do |c|
      #ca = Case.find_or_initialize_by(client_ref_no: c['client_ref_no'])
      #ca.attributes = c
      #ca.save!
      c.stringify_keys!
      @cases = Case.find_by(client_ref_no: c['client_ref_no'])
      if @cases.present?
        @cases.update_attributes(c)
      else
        Case.open.create!(c)
      end
    end
    redirect_to staff_cases_path
  end

  def show 
    @case = Case.find(params[:id])
    @case_investigation = CaseInvestigation.find_by(case_id: @case.id, user_id:  @case.user_id)
    unless @case_investigation.present?
      @case_investigation = CaseInvestigation.new
    end
  end


  def upload_attachment
    attachment_params.each do |attach_param|
      if attach_param[:document].present?
        attachment = Attachment.new(attach_param)
        attachment.case_id = @case.id
        attachment.user_id = current_user.id
        attachment.save!
      end
    end
  end

  def upload_verification_detail
    @case = Case.find(params[:id])
    @verified_case = CaseInvestigation.find_by(case_id: @case.id, user_id: @case.user_id)

    if @verified_case.present?
      @verified_case = @verified_case.update_attributes(case_investigation_params)
    else
      @verified_case = CaseInvestigation.new(case_investigation_params)
      @verified_case.case_id = @case.id
      @verified_case.user_id = current_user.id
      @verified_case.save!
    end
    redirect_to staff_case_path(@case)
  end


  def mark_approved
    @case = Case.find(params[:id])
    if @case.status == "verification_completed"
      @case.staff_id = current_user.id
      @case.status = "approved"
      @case.save!
    end
      redirect_to staff_cases_path
  end

  def search
    params[:search] ||= {}
    @users = User.where(role: 'executive')
    @cases = Case.where("1=1")
    @cases = @cases.where(status: params[:search][:status]) if params[:search][:status].present?
    
    if params[:search][:query].present?
      query = '%' + params[:search][:query] + '%'
      @cases = @cases.where("address ILIKE ?", query)
    end

    if params[:search][:client_id].present?
      @cases = @cases.where(client_id: params[:search][:client_id])
    end

    if params[:search][:user_id].present?
      @cases = @cases.where(user_id: params[:search][:user_id])
    end

    if params[:search][:case_type].present?
      @cases = @cases.where(case_type: params[:search][:case_type]) 
    end

    respond_to do |format|
      format.html { render action: :index }
      format.csv { send_data @cases.to_csv, filename: "cases-#{Date.today}.csv" }
    end
  end




  def bulk_assignment
    if params[:case_ids].present?
      params[:case_ids].each do |case_id|
        c = Case.find(case_id)
        c.update_attributes user_id: params[:user_id], status: "in_progress"
      end
    else
      flash[:notice] = "select cases"
    end
    redirect_to staff_cases_path
  end

  def crop_attachment
    @attachment = @case.attachments.find(params[:attachment_id])
  end

  def attachment_params
    params.require(:attachments).map{|p| p.permit(:document)}
  end

  def case_investigation_params
    params.require(:case_investigation).permit(:visited_at, :address_changed, :land_mark, :locality, :accommodation_type, :duration_of_stay, :building_color, :building_type, :id_number, :respondent_name, :respondent_relationship, :respondent_contact_no, :police_station_name, :remarks, extras: {})
  end

  def find_case
    @case = Case.find(params[:id])
  end
end
