class Staff::DashboardController < ApplicationController

  def show
    @cases = Case.all
    @executives = User.where(role: 'executive')
    @staffs = User.where(role: 'staff')
  end



  def reports
  end

  def staffs_report
    @user = User.find(params[:staff_id])
    @cases = Case.where(staff_id: @user.id, status: ["approved"])
    render action: :reports
  end

  def executives_report
    @user = User.find(params[:executive_id])
    @cases = Case.where(user_id: @user.id, status: ["approved", "verification_completed"])
    render action: :reports
  end

end
