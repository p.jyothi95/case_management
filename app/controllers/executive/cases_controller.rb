class Executive::CasesController < ApplicationController

  def new
  end

  def index
    @cases = Case.where(user_id: current_user, status: "in_progress").order(created_at: :desc)
  end

  def show
    @case = Case.find(params[:id])
    @case_investigation = CaseInvestigation.find_by(case_id: @case.id, user_id:  @case.user_id)
    unless @case_investigation.present?
      @case_investigation = CaseInvestigation.new
    end
  end

  def template  
    @case = Case.find(params[:id])
    render layout: 'print'
  end

  def upload_verification_detail
    @case = Case.find(params[:id])
    @verified_case = CaseInvestigation.find_by(case_id: @case.id, user_id: @case.user_id)
    
    if attachment_params.present?
      upload_attachment
    end

    if params[:case_investigation].present?
      if @verified_case.present?
        @verified_case = @verified_case.update_attributes(case_investigation_params)
      else
        @verified_case = CaseInvestigation.new(case_investigation_params)
        @verified_case.case_id = @case.id
        @verified_case.user_id = @case.user_id
        @verified_case.save!
      end
    end

    redirect_back(fallback_location: executive_case_path(@case))
  end

  def mark_verification_completed
    @case = Case.find(params[:id])
    @case.status = "verification_completed"
    @case.save!
    redirect_to executive_cases_path
  end

  def verification_completed
    @cases = Case.where(user_id: current_user, status: ["approved", "verification_completed"])
  end

  def redo
    @case = Case.find(params[:id])
    @case.status = "in_progress"
    @case.save!
    redirect_to executive_case_path(@case)
  end

  def set_untraceable
    @case = Case.find(params[:id])
    @untraceable_case = CaseInvestigation.find_by(case_id: @case.id, user_id: @case.user_id)
    
    if @untraceable_case.present?
      @untraceable_case.update_attributes(case_investigation_params)
    else 
      @untraceable_case  = CaseInvestigation.new(case_investigation_params)
      @untraceable_case.case_id = @case.id
      @untraceable_case.user_id = current_user.id
      @untraceable_case.save!
    end

    @case.status = "verification_completed"
    @case.is_untraceable = true
    @case.save!
    redirect_to executive_case_path(@case)
  end

  def print_all
    @cases = Case.where(user_id: current_user, status: "in_progress")
    render layout: 'print'
  end

  private

  def attachment_params
    params.require(:attachments).map{|p| p.permit(:document)} if params[:attachments].present?
  end

  def case_investigation_params
    params.require(:case_investigation).permit(:visited_at, :address_changed, :land_mark, :locality, :accommodation_type, :duration_of_stay, :building_color, :building_type, :id_number, :respondent_name, :respondent_relationship, :respondent_contact_no, :police_station_name, :remarks, :untraceable, extras: {})
  end

  def upload_attachment
    attachment_params.each do |attach_param|
      if attach_param[:document].present?
        attachment = Attachment.new(attach_param)
        attachment.case_id = @case.id
        attachment.user_id = current_user.id
        attachment.save!
      end
    end
  end

end
