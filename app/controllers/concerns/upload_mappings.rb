module UploadMappings
  def upload_mapping_securitas_address #Securitas address, format1
      return false if !valid_securitas_address_format

      @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          candidate_name: row['Name'],
          client_ref_no: row['CompanyRefNo'],
          father_name: row['Father Name'],
          address: row['address'],
          pin_code: row['AV_Pincode'],
          location: row['Location'],
          state: row['StateName'],
          contact: row['MobileNo'],
          received_date: row['Allocation Date'].presence || Date.today
        }
      end
  end

  def valid_securitas_address_format
    header = @csv.headers
    required_columns = ['Name', 'CompanyRefNo', 'Father Name', 'address', 'AV_Pincode', 'Location', 'StateName', 'MobileNo', 'Allocation Date']
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for Securitas address"
      return false
    end
    return true
  end

  def upload_mapping_securitas_employment #Securitas employee, format2
    return false if !valid_securitas_employment_format

    @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          client_ref_no: row['Securitas Ref No.'],
          candidate_name: row['Candidate Name'],
          company_name: row['Company name'],
          address: row['Address'],
          location: row['Location'],
          received_date: row['Received Date']  || Date.today
        }
      end
  end

  def valid_securitas_employment_format
    header = @csv.headers
    required_columns = ['Securitas Ref No.', 'Candidate Name', 'Company name', 'Address', 'Location', 'Received Date']
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for Securitas employement"
      return false
    end
    return true
  end

  def upload_mapping_footprint_address #Footprints address, Format3
      return false if !valid_footprint_address_format
      @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          client_ref_no: row['Check Id'],
          candidate_name: row['Name'],
          address: row['Address'],
          contact: row['Contact No'],
          received_date: row['CS SENT DT'] || Date.today
        }
      end
  end

  def valid_footprint_address_format
    header = @csv.headers
    required_columns = ['Check Id', 'Name', 'Address', 'Contact No', 'CS SENT DT']
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for Footprnt address"
      return false
    end
    return true
  end

  #def upload_mapping_footprints_employment #Footprints employee, Format4

   # @csv.collect do |row|
    #  {
     #   client_id: params[:client_id],
      #  case_type: params[:case_type],
       # candidate_name: row['Candidate name'],
        #address: row['Address'],
        #received_date: row['Sent date']
      #}
    #end
  #end

  def upload_mapping_pinkerton_address #Pinkerton address, Format5
      return false if !valid_pinkerton_address_format

      @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          candidate_name: row['Candidate Name'],
          client_ref_no: row['Order ID'],
          father_name: row['Father Name'],
          address: row['Address'],
          location: row['City'],
          state: row['State'],
          contact: row['Contact No'],
          dob: row['Date of Birth'],
          received_date: Date.parse(row['Allocated date']) || Date.today
        }
      end
  end

  def valid_pinkerton_address_format
    header = @csv.headers

    required_columns = ['Candidate Name', 'Order ID', 'Father Name', 'Address', 'City', 'State', 'Contact No', 'Date of Birth', 'Allocated date'  ]
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for Pinkerton address"
      return false
    end
   return true
  end

  def upload_mapping_pinkerton_employment #Pinkerton employee, Format6
      return false if !valid_pinkerton_employee_format

      @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          client_ref_no: row['Order id'],
          emp_duration: row['Duration'],
          designation: row['Designation'],
          candidate_name: row["Subject's Name"],
          company_name: row['Name of Organization'],
          address: row['Address to be verified'],
          received_date: Date.today
        }
      end
  end

  def valid_pinkerton_employee_format
    header = @csv.headers
    required_columns = ['Order id', 'Duration', 'Designation', "Subject's Name", 'Name of Organization', 'Address to be verified' ]
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for Pinkerton employment"
      return false
    end
    return true
  end

  def upload_mapping_asd_address #ASD address, Format7
      return false if !valid_asd_address_format

      @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          company_name: row["Client name"],
          client_ref_no: row["ASD ID"],
          candidate_name: row["Applicant's Name"],
          father_name: row["Father's Name"],
          address: row[" Address"],
          contact: row["Applicant Contact Detail"],
          received_date: row['Allocation Date'] || Date.today
        }
      end
  end

  def valid_asd_address_format
    header = @csv.headers.map &:strip
    required_columns = ['Client name', 'ASD ID', "Applicant's Name", "Father's Name", 'Address',
      'Applicant Contact Detail', 'Allocation Date' ]
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for ASD address"
      return false
    end
    return true
  end

  def upload_mapping_asd_employment #ASD employee, Format8
    #return false if !valid_asd_employee_format  #check format before push

      @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          client_ref_no: row["ASD ID"],
          candidate_name: row["APPLICANT NAME"],
          company_name: row["ORGANIZATION NAME"],
          address: row["Address"],
          designation: row['DESIGNATION'],
          emp_code: row['EMPCODE'],
          tenture: row['TENURE'],
          received_date: row['allow date'] || Date.today
        }
      end
  end

  def valid_asd_employee_format
    header = @csv.headers
    raise header.inspect
    required_columns = ['ASD ID', "APPLICANT NAME", "ORGANIZATION NAME", 'Address', 'DESIGNATION', 'EMPCODE', 'TENURE', 'allow date' ]
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for ASD employement"
      return false
    end
    return true
  end

  def upload_mapping_decode_address #Decode address, Format9
      return false if !valid_decade_address_format

      @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          client_ref_no: row['Ref. Number'],
          company_name: row['Client Name'],
          candidate_name: row['Candidate Name'],
          father_name: row['Fathers Name'],
          address: row['Permanent Address'],
          contact: row['Phone Numbers'],
          received_date: Date.parse(row['allow date']) || Date.today
        }
      end
  end

  def valid_decade_address_format
    header = @csv.headers
    required_columns = ['Ref. Number', 'Client Name', 'Candidate Name', 'Fathers Name', 'Permanent Address', 'Phone Numbers', 'allow date']
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for Decade address"
      return false
    end
    return true
  end

  def upload_mapping_acheck_address # Acheck address, format10
      return false if !valid_acheck_address_format

      @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          client_ref_no: row['Client Ref ID'],
          company_name: row['Client Name'],
          candidate_name: row['Candidate Name'],
          father_name: row["Father's Name"],
          address: row['Address'],
          contact: row['Contact Details'],
          received_date: row["SENT DATE"] || Date.today
        }
      end
  end

  def valid_acheck_address_format
    
    header = @csv.headers
    required_columns = ['Client Ref ID', 'Client Name', 'Candidate Name', "Father's Name", 'Address', 'Contact Details', 'SENT DATE']
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for Acheck address"
      return false
    end

    return true
  end

  def upload_mapping_acheck_employment # Acheck employee, format11
      return false if !valid_acheck_employee_format 

      @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          client_ref_no: row['Client Ref ID'],
          company_name: row['COMPANY NAME'],
          candidate_name: row['CANDIADTE MANE'],
          address: row['ADDRESS'],
          contact: row['CONTACTNO'],
          received_date: row["SENT DATE"] || Date.today
        }
      end
  end

  def valid_acheck_employee_format

    header = @csv.headers
    required_columns = ['Client Ref ID', 'COMPANY NAME', 'CANDIADTE MANE', 'ADDRESS', 'CONTACTNO', 'SENT DATE']
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for Acheck employee"
      return false
    end

    return true
  end

  def upload_mapping_authbridge_employment # Authbridge employee, format12
      return false if !valid_authbridge_employee_format
      @csv.collect do |row|
        {
          client_id: params[:client_id],
          case_type: params[:case_type],
          client_ref_no: row['ARS'],
          company_name: row['COMPANY NAME'],
          address: row['COMPANY ADDRESS'],
          pin_code: row['Pin code'],
          location: row['City'],
          state: row['State'],
          tenture: row['Tenure'],
          designation: row['Designation'],
          emp_code: row['Employee Code'],
          reason_for_leaving: row['Reason For Leaving'],
          last_salary: row['Last drawn salary by the Applicant (Annual Gross)'],
          candidate_name: row['Subject Name'],
          contact: row['Contact No.'],
          received_date: Date.today
        }
      end
  end

  def valid_authbridge_employee_format
    header = @csv.headers
    required_columns = ['ARS', 'COMPANY NAME', 'COMPANY ADDRESS']
    unless (required_columns - header).empty?
      flash[:notice] = "Not a valid format for Authbridge employee"
      return false
    end

    return true
  end
end
