class ApplicationMailer < ActionMailer::Base
  default from: "p.jyothi95@gmail.com"
  layout 'mailer'
end
