Rails.application.routes.draw do
  devise_for :users
  root to: 'staff/dashboard#show' 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :staff do
    resource :dashboard, controller: :dashboard do
      collection do
        get :tats
        get :reports
        get :staffs_report
        get :executives_report
      end
    end
      resources :cases do
      collection do
        get :upload_file
        post :upload_mapping
        post :upload
        get :search
        put :bulk_assignment
        get :out_of_tats
        get :today_tats
        get :tomorrow_tats
      end
      member do
        get  :mark_approved
        post :upload_attachment
        post :upload_verification_detail
        get :crop_attachment
      end

      resources :attachments
    end
    resources :clients
    resources :users
  end

  namespace :executive do
    resources :cases do
      collection do
        get :verification_completed
        get :print_all
      end
      member do
        post :upload_attachment
        get :template
        post :upload_verification_detail
        get  :mark_verification_completed
        get :redo
        post :set_untraceable
      end
    end
  end
end
