# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191018140945) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "attachments", force: :cascade do |t|
    t.integer "case_id"
    t.integer "user_id"
    t.string "document_file_name"
    t.string "document_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cropped_image_file_name"
  end

  create_table "case_investigations", force: :cascade do |t|
    t.date "visited_at"
    t.text "address_changed"
    t.string "land_mark"
    t.string "locality"
    t.string "accommodation_type"
    t.string "duration_of_stay"
    t.string "building_color"
    t.string "building_type"
    t.string "id_number"
    t.string "respondent_name"
    t.string "respondent_relationship"
    t.string "respondent_contact_no"
    t.string "police_station_name"
    t.text "remarks"
    t.hstore "extras"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.integer "case_id"
    t.string "untraceable"
  end

  create_table "cases", force: :cascade do |t|
    t.integer "client_id"
    t.string "client_ref_no"
    t.string "candidate_name"
    t.string "company_name"
    t.string "address"
    t.string "case_type"
    t.date "received_date"
    t.string "emp_duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.string "status"
    t.string "location"
    t.string "state"
    t.date "tenture"
    t.string "designation"
    t.string "emp_code"
    t.string "reason_for_leaving"
    t.string "last_salary"
    t.string "contact"
    t.string "pin_code"
    t.string "father_name"
    t.string "dob"
    t.integer "staff_id"
    t.boolean "is_untraceable", default: false
  end

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "client_format"
    t.integer "tat"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "address"
    t.string "role"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "l_name"
    t.date "dob"
    t.string "photo_file_name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
