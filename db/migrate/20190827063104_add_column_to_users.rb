class AddColumnToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :l_name, :string
    add_column :users, :dob, :date
    add_column :users, :photo_file_name, :string
  end
end
