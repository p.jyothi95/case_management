class CropedImageToAttachments < ActiveRecord::Migration[5.1]
  def change
  	add_column :attachments, :cropped_image_file_name, :string
  end
end
