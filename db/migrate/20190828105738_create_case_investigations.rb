class CreateCaseInvestigations < ActiveRecord::Migration[5.1]
  def change
    enable_extension "hstore"
    create_table :case_investigations do |t|
      t.date :visited_at
      t.text :address_changed
      t.string :land_mark
      t.string :locality
      t.string :accommodation_type
      t.string :duration_of_stay
      t.string :building_color
      t.string :building_type
      t.string :id_number
      t.string :respondent_name
      t.string :respondent_relationship
      t.string :respondent_contact_no
      t.string :police_station_name
      t.text :remarks
      t.hstore :extras
      t.timestamps
    end
  end
end
