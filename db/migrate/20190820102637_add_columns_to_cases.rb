class AddColumnsToCases < ActiveRecord::Migration[5.1]
  def change
    add_column :cases, :user_id, :integer
    add_column :cases, :status, :string
    add_column :cases, :assign_to, :string
  end
end
