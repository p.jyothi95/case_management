class AddColumnsCases < ActiveRecord::Migration[5.1]
  def change
  	rename_column :cases, :assign_to, :location
  	add_column :cases, :state, :string
  	add_column :cases,:tenture, :date
  	add_column :cases, :designation, :string
  	add_column :cases, :emp_code, :string
  	add_column :cases, :Reason, :string
  	add_column :cases, :last_salary, :string
  	add_column :cases, :contact, :string
  end
end
