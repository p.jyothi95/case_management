class RenameColumnCases < ActiveRecord::Migration[5.1]
  def change
    rename_column :cases, :Reason, :reason_for_leaving
  end
end
