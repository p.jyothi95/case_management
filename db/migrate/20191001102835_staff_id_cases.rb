class StaffIdCases < ActiveRecord::Migration[5.1]
  def change
    add_column :cases, :staff_id, :integer
  end
end
