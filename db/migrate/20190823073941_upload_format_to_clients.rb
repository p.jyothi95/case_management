class UploadFormatToClients < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :upload_format, :string
  end
end
