class PinCodeToCases < ActiveRecord::Migration[5.1]
  def change
  	add_column :cases, :pin_code, :string
  	add_column :cases, :father_name, :string
  end
end
