class CreateAttachments < ActiveRecord::Migration[5.1]
  def change
    create_table :attachments do |t|
      t.integer :case_id
      t.integer :user_id
      t.string :document_file_name
      t.string :document_type
      t.timestamps
    end
  end
end
