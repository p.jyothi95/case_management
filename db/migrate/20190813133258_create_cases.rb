class CreateCases < ActiveRecord::Migration[5.1]
  def change
    create_table :cases do |t|
      t.integer :client_id
      t.string :client_ref_no
      t.string :candidate_name
      t.string :company_name
      t.string :address
      t.string :case_type
      t.date :received_date
      t.string :emp_duration
      t.timestamps
    end
  end
end
