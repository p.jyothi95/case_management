class UntraceableToCaseinvestigation < ActiveRecord::Migration[5.1]
  def change
    add_column :case_investigations, :untraceable, :string
  end
end
