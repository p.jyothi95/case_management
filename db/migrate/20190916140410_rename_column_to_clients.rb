class RenameColumnToClients < ActiveRecord::Migration[5.1]
  def change
    rename_column :clients, :upload_format, :client_format
    remove_column :clients, :template_type
  end
end
