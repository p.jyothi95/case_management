# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def create_user(data)
  return if User.find_by(email: data[:email]).present?

  user = User.create! data.merge(password: '123456', password_confirmation: '123456')
  puts "Created #{user.role} user: #{user.email}"
end

create_user name: 'Admin', role: 'admin', email: 'admin@xcm.com'
create_user name: 'Staff One', role: 'staff', email: 'staff1@xcm.com'
create_user name: 'Executive One', role: 'executive', email: 'executive1@xcm.com'


def create_client(data)
  return if Client.find_by(email: data[:email]).present?

  client = Client.create! data.merge(tat: 3)
  puts "Created client: #{client.email}"
end

create_client name: 'Securitas', email: 'securitas@xcm.com', client_format: 'format1'
create_client name: 'Footprints', email: 'footprints@xcm.com', client_format: 'format3'
create_client name: 'Pinkerton', email: 'pinkerton@xcm.com', client_format: 'format5'
create_client name: 'ASD', email: 'asd@xcm.com', client_format: 'format7'
create_client name: 'Decode', email: 'decode@xcm.com', client_format: 'format9'
create_client name: 'Acheck', email: 'acheck@xcm.com', client_format: 'format10'
create_client name: 'Authbridge', email: 'authbridge@xcm.com', client_format: 'format12'
